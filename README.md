Hi there, I'm Sagor - Owner of <a rel="dofollow noreferrer noopener" href="https://codingspoint.com/"> CodingsPoint </a> 👋


<ul>
<li>🔭 Hi, I’m @shahriar_sagor</li>
<li>🌱 I’m currently learning and working on Vue js 🤣</li>
<li>👯 I’m looking to collaborate some big projects</li>
<li>🥅 2022 Goals: Contribute to Open Source projects</li>
<li>⚡ Fun fact: I love to learn history</li>
</ul>

<h3>Some Blog</h3>
<ul>
<li > <a href="https://codingspoint.com/laravel-8-toastr-notifications-using-yoeunes-toastr-package/"> Laravel 8 Toastr Notifications using yoeunes/toastr package </a> </li>
</ul>
